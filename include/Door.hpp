#ifndef DOOR_HPP
#define DOOR_HPP

#include <tinyfsm.hpp>

// 4 EVENTS Open Close Locking Unlocking
/*struct State : tinyfsm :: Event
{
   static bool L_state;
};*/

struct Open : tinyfsm::Event
{
};
struct Close : tinyfsm::Event
{
};
struct Locking : tinyfsm::Event
{
};
struct Unlocking : tinyfsm::Event
{
};

// Base Class

class Door : public tinyfsm::Fsm<Door> //Door name change
{

 //friend class tinyfsm::Fsm<Door>;
   // deafult reaction

 public:
   static bool D_state;
   static bool L_state;
   // void func();
   //void react(tinyfsm::Event const &){};
   //friend class tinyfsm::Fsm<Door>;
   virtual void react(Open const &){};
   virtual void react(Close const &){};
   virtual void react(Locking const &){};
   virtual void react(Unlocking const &){};
   virtual void entry(void){};
   virtual void exit(void){};
   void         current_state();
 
   bool get_L_State()
   {
      return L_state;
   }

   bool get_D_State()
   {
      return D_state;
   }

    Door()
   {                    // D_State=1 OPEN : D_State=0 CLOSE                        // L_state=0 UNLocked : L_state=1 LOCKED
      D_state = false;  // Closed
      L_state = true;   // Locked
   }

   // extern
};

bool Door::D_state;
bool Door::L_state;
#endif
