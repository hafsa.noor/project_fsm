
#include "dispatch.hpp"

#include <iostream>

// using fsm_handle = Door;

int main()
{

   // Event Objects
   Open      open;
   Close     close;
   Locking   locking;
   Unlocking unlocking;

   char input;
   

   cout << "Door is Currently Locked, Please enter a value to perform an action on the Door" << endl;
   fsm_list::start();
   try{
      while (1)
      {

         cout << "o=Open" << endl << "c=Close" << endl << "l=Lock" << endl << "u=UnLock" << endl << "e=Exit" << endl;
         cin >> input;
         cout << endl;

         switch (input)
         {
         case 'o':
            send_event(open);
            break;
         case 'c':
            send_event(close);
            break;
         case 'l':
            send_event(locking);
            break;
         case 'u':
            send_event(unlocking);
            break;
         case 'e':
            return 0;
         default:
            throw invalid_argument ("Wrong input");
            //break;
         }
      }
   }
   catch(invalid_argument& ) {
    std::cerr << "Error: Invalid Argument \n";
   } 
   return 1;
}